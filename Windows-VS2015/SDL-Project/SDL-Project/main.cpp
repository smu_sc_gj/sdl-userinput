/*
  Based on http://lazyfoo.net/SDL_tutorials/
  Used with permission. 
*/

//Include SDL library
#if defined(_WIN32)
    #include "SDL.h"
#else
    #include "SDL/SDL.h"
#endif

int main( int argc, char* args[] )
{
    //Event driven programming variables
    SDL_Event event;
    int quit= 1;  //false

    //Declare pointers to surface structures
    SDL_Surface* screen = NULL;

    //Initialise SDL
    //NOTE: SDL_INIT_EVERYTHING is a constant defined by SDL
    SDL_Init( SDL_INIT_EVERYTHING );

    //Surface structure we'll use for the window - screen
    //This is essentially the back buffer. 
    screen = SDL_SetVideoMode( 640, 480, 32, SDL_SWSURFACE );


    //Game loop
    while(quit == 1)
    { 
        //If there's events to handle 
        if( SDL_PollEvent( &event ) ) 
        { 
            switch(event.type) 
            { 
                case SDL_QUIT: //If the user has closed(x) out the window 
                    //Quit the program 
                    quit = 0;
                break; 
                //If the left mouse button was pressed
                case SDL_MOUSEBUTTONDOWN:  
                    if(event.button.button == SDL_BUTTON_LEFT) 
                        quit = 0;
                break;
            
                //If the escape key is pressed
                case SDL_KEYDOWN:
                    if(event.key.keysym.sym == SDLK_ESCAPE)
                        quit = 0;
                break;  
            }
        }

        //Refresh the screen (replace with backbuffer. 
        SDL_Flip( screen );
    }

    //Shutdown SDL - clear up resorces etc. 
    SDL_Quit();

    return 0;
}
